"""
Script to calculate surface runoff via the Curve Number Method.

Its a prerequisite that a raster has been prepared with curve numbers (CN). If
intended to be used with Malstroem, the CN raster must have same extent and 
resolution as the Digital Elevation Model.

Moreover a raster must be prepared prior to this script with rainfall depths.
Again the extent and resolution must match the DEM (and thereby also the CN raster)

This script will produce a raster with runoff in milimeters.
   
"""

import os
import os.path
import numpy as np
import numpy.ma as ma
import json

os.environ['PROJ_LIB'] = 'C:\\Users\\WaterITech\\anaconda3\\Library\\share\\proj'
os.environ['GDAL_DATA'] = 'C:\\Users\\WaterITech\\anaconda3\\Library\\share'

from osgeo import gdal
from osgeo import gdalconst


configPath = "config.json"
config=json.load(open(configPath))

rootPath = config["dataRoot"]

###############################################################################
# Calculating runoff
###############################################################################

#-----------------------------------------------------------------------------#
# reading in the Curve Number tif:
#-----------------------------------------------------------------------------#
# path to curve number tif
CurveNumberTif = os.path.join(rootPath,config["curveNumberRaster"]).replace('\\', '/')

match_ds = gdal.Open(CurveNumberTif, gdalconst.GA_ReadOnly)
b1 = match_ds.GetRasterBand(1)
CNIn = b1.ReadAsArray().flatten()    
NoData = b1.GetNoDataValue()
match_geotrans  = match_ds.GetGeoTransform()
# We want a section of source that matches this:
match_proj                  = match_ds.GetProjection()
match_geotrans              = match_ds.GetGeoTransform()
match_wide                  = match_ds.RasterXSize
match_high                  = match_ds.RasterYSize

match_ds = None

#-----------------------------------------------------------------------------#
# Masking tifs to optimize calculations and calibrating CN numbers
#-----------------------------------------------------------------------------#

Dataindexes = np.array((CNIn == NoData), dtype=bool)
# we create a masked array to speed up calculations. To get back to a 
# regular numpy array use the call: pp = ma.getdata(x)

CNTif       = ma.array(CNIn, mask=Dataindexes)  
CNIn = None
# calibrating the values according to the factor defined in the config
curveNumberFactor   = config["curveNumberFactor"] 

CNTifcal = np.multiply(CNTif, curveNumberFactor)

# we check that the Curve Numbers are not outside limits after adjustments
CNTifcal[(CNTifcal >= 100)] = 100
CNTifcal[(CNTifcal < 0)] = 0

#-----------------------------------------------------------------------------#
# reading in rainfall tifs:
#-----------------------------------------------------------------------------#    

for key, val in config["rainEvents"].items():    
    print("processing: %s" %key)
    
    rainBruttoInFile   = os.path.normpath(os.path.join(rootPath,val["inFile"]))
    rainNettoOutFile   = os.path.normpath(os.path.join(rootPath,val["outFile"]))

    rain_ds = gdal.Open(rainBruttoInFile, gdalconst.GA_ReadOnly)
    b1      = rain_ds.GetRasterBand(1)
    rainIn  = b1.ReadAsArray().flatten()    
    NoData  = b1.GetNoDataValue()
    
    rain_ds = None

    # Masking both tifs to optimize calculations
    RainfallTif = ma.array(rainIn, mask=Dataindexes)    
        
    #-------------------------------------------------------------------------#
    # actual calculations
    #-------------------------------------------------------------------------#
    CN = CNTifcal
    lambdaConst = config["lambdaConst"]
    P = RainfallTif
    
    S = ((25400/(CN)) - 254)
    Ia = lambdaConst * S
    Q = (P - Ia)**2 / (P - Ia + S)
    
    #-------------------------------------------------------------------------#
    # exporting netto precipitation
    #-------------------------------------------------------------------------#
    print("exporting the tif")
    nettoRainfallData = ma.getdata(Q)
    nettoRainfallData[np.isnan(nettoRainfallData)] = 0
    
    # reshape the numpy array to a 2d array:
    nettoRain2D = nettoRainfallData.reshape(match_high,match_wide)
    
    # checking if file exists:
    if os.path.exists(rainNettoOutFile):
        os.remove(rainNettoOutFile)
    
    
    driver = gdal.GetDriverByName("GTiff")
    outdata = driver.Create(rainNettoOutFile, match_wide, match_high, 1, gdalconst.GDT_Float32, options=['COMPRESS=DEFLATE'])
    outdata.SetGeoTransform(match_geotrans)##sets same geotransform as input
    outdata.SetProjection(match_proj)##sets same projection as input
    outdata.GetRasterBand(1).WriteArray(nettoRain2D)
    outdata.GetRasterBand(1).SetNoDataValue(0)##if you want these values transparent
    outdata.FlushCache() ##saves to disk!!
    outdata = None


"""
This part is unused at the moment:

def InfiltrationRainCal(P,Q):
    
    infilCal = P - Q
    
    if infilCal < 0:
        infilCal = 0
    
    return infilCal
    
    
InfiltrationRainCalFunc = np.vectorize(InfiltrationRainCal)
InfiltrationRainCalArray =  InfiltrationRainCalFunc(RainfallTif,Qarray)

"""


























