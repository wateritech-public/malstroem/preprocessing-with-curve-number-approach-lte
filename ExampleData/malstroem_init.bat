rem Inputs
set DEMFILE="\DEMadjusted.tif"
set RAINTIF=RainBruttoConstant.tif

rem Raster outputs
set LABELSFILE=bluespots.tif
set FILLEDFILE=filled.tif
set DEPTHSFILE=depths.tif
set FLOWDIRFILE=flowdir.tif
set ACCUMFILE=accum.tif
set WSHEDSFILE=wsheds.tif
set FINALBLUESPOTSFILE=final_bluespots.tif 
set FINALDEPTHSFILE=final_depths.tif

rem Vector outputs
set OUTVECTOR=results.gpkg
set PPTS=pourpoints
set NODES=nodes
set INITVOL=initvolumes
set FINALVOLS=finalvolumes
set HYPS=hyps
set FINALLEVELS=finallevels
set FINALPOLYGONS=finalpolygons

