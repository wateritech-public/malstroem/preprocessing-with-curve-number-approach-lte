# About the code:
**Script to calculate surface runoff via the Curve Number Method and subsequently Bluespots via Malstrøm**

<img src="Pics/ConceptualFigure.png"  width="80%" height="80%">

## Acknowledgements:
At WaterITech we take pride in sharing knowlegde and data that are crucial for our society's transformation towards a more sustainable use and management of the world’s precious water resources.​
We hope this code will be helpful for your particular challenge associated to water. It is distributed with a GNU General Public License, thus with WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

**Authors:** Anders Nielsen & Dennis Trolle

**Version:** 0.1

--------

# Preprocessing with Curve Number Approach LTE


## Installation:
The script utilizes standard python libraries and in addition ```python gdal``` which needs to be installed prior to execution.

To execute Malstrøm simulations a seperate python environment needs to be created (see section further below)


## Configuration:
The script utilizes as json configuration file, where variables and paths are set. Remember to change e.g. naming and paths to match the local machine. 
```json
{
"dataRoot"			: "--set the root to local preferences--",
"curveNumberRaster" : "CurveNumbers.tif",
"lambdaConst"       : 0.2,
"curveNumberFactor" : 1.0,

"rainEvents" 		: {"distributed" : {"inFile"     : "RainBruttoDistributed.tif",
										"outFile"    : "RainNettoDistributed.tif" 
									   },
					   "constant"    : {"inFile"     : "RainBruttoConstant.tif",
										"outFile"    : "RainNettoConstant.tif" 
                                       }
					  }
}
```
## Curve Number inputs:
Its a prerequisite that a raster has been prepared with curve numbers (CN) for the area of interest. If intended to be used with Malstrøm, the CN raster must have same extent and resolution as the Digital Elevation Model.
An example of a curve number raster is included as example data, and displayed here:

<img src="Pics/OnlyCN.png"  width="60%" height="60%">

This particular raster has been derived from the [SWAT+](https://swat.tamu.edu/software/plus/) database based on the unique combination of soil properties and land use.

**Soil properties** in this examples rely on the soil texture map by [Adhikari et al. (2013)]( https://doi.org/10.2136/sssaj2012.0275) provided by Aarhus University (AU).

<img src="Pics/OnlySoil.png"  width="60%" height="60%">

**Land use** in this example rely on the [Basemap](https://envs.au.dk/faglige-omraader/samfund-miljoe-og-ressourcer/arealanvendelse-og-gis/basemap) by Aarhus University (AU) 

<img src="Pics/OnlyLanduse.png"  width="60%" height="60%">

Moreover, a raster must be prepared prior to this script with rainfall depths. Included with this example is rainfall based on DMI 1x1 km gridded data for an rainfall event the 19. October 2021 represented as the distributed grid depths and as the averaged rainfall depth for the project area.

<img src="Pics/OnlyPrecip.png"  width="60%" height="60%">

The rainfall data is based on an analysis of events for the entire watershed of [Vejle and Grejs](https://asap-forecast-storage.one/datasources/WaterWebToolsDemos/Malstroem/Rainfall.html). Again the extent and resolution of an rainfall raster must match the DEM (and thereby also the CN raster)

--------

**Derived output:**

Ultimately the script will produce a raster with runoff in milimeters accounting for losses via the curve number method.

<img src="Pics/OnlyDistributedPrecip.png"  width="60%" height="60%">


To continue with malstrøm simulations, the only additional input necessary along with rainfall is a Digital Elevation Model (DEM). An example is inluded and displayed here:
   
<img src="Pics/OnlyDEMwithGRID.png"  width="60%" height="60%"> 

This DEM (raw data is available [here](https://dataforsyningen.dk/data/930) ) have been processed to create a hydro representative model with the [hydro adjust tool](https://github.com/SDFIdk/dhm-hydro-adjust).

--------

# Malstrøm


This documentation is partly forked from the malstroem git repository.
Please see the following link for further information:
[![ref](https://github.com/Septima/malstroem)]


## About the Malstrøm
Tools for screening of bluespots and water flow between bluespots.

Malstrøm is based on the ideas presented by Balstrøm and Crawford (2018).

Balstrøm, T. & Crawford, D. (2018): Arc-Malstrøm: A 1D hydrologic screening method for stormwater assessments based on geometric networks-. Computers & Geosciences vol. 116, July 2018, pp. 64-73., [doi:10.1016/j.cageo.2018.04.010](https://doi.org/10.1016/j.cageo.2018.04.010)

[![Build status](https://ci.appveyor.com/api/projects/status/hhnnd65moi0fl71w/branch/master?svg=true)](https://ci.appveyor.com/project/Septima/malstroem/branch/master)

## Features

malstroem provides command line tools and a python api to calculate:

* Depressionless (filled, hydrologically conditioned) terrain models
* Surface flow directions
* Accumulated flow
* Blue spots
* Local watershed for each bluespot
* Pour points (point where water leaves blue spot when it is filled to its maximum capacity)
* Flow paths between blue spots
* Rain incident can be specified either as a single scalar value (x mm) or as a raster
* Fill volume at specified rain incident
* Spill over between blue spots at specified rain incident
* Approximate water level and extent of partially filled bluespots

Assumptions
-----------
malstroem makes some assumptions to simplify calculations. The most important ones which you must be aware of:

* malstroem assumes that the terrain is an impermeable surface. This may change in a later version.
* malstroem does not know the concept of time. This means that the capacity of surface streams is infinite no matter the
width or depth. Streams wont flow over. The end result is the situation after infinite time, when all water has reached
its final destination.
* Water flows from one cell to one other cell (the D8 method).
* Partially filled bluespots are assumed to be filled in cell Z order (from lowest to highest cells). No attempt is made 
to model how water actually flows within the bluespots.

Installation
------------

### Installing on Windows
1. Install `Microsoft Visual C++ 14.2 standalone: Build Tools for Visual Studio 2019` as described [here](https://wiki.python.org/moin/WindowsCompilers#Microsoft_Visual_C.2B-.2B-_14.2_standalone:_Build_Tools_for_Visual_Studio_2019_.28x86.2C_x64.2C_ARM.2C_ARM64.29).
   A direct link to installation: [here](https://visualstudio.microsoft.com/visual-cpp-build-tools/)

2. Download and install [miniconda3 64bit](https://docs.conda.io/en/latest/miniconda.html) for you Windows. (If you have other versions of python installed on your system, make sure to untick `Register Anaconda as my default Python`)

3. Download the malstroem dependencies file [environment.yml](https://github.com/Septima/malstroem/raw/master/environment.yml). Note the path to the downloaded file like `C:\Users\###\Downloads\environment.yml`.

4. From the start menu, search for and open `Anaconda Prompt`.

5. In the Anaconda Prompt run
```
conda env create -f C:\Users\###\environment.yml
```
Where `C:\Users\###\Downloads\environment.yml` is the path to your downloaded copy of `environment.yml`.

6. In the Anaconda Prompt run
```
conda activate malstroem
```
This activates the `malstroem` environment in the current prompt. Your prompt should show something like
```
(malstroem) C:\Users\###\>
```
Indicating that `malstroem` is the currently active environment.

7. Install `malstroem` into the environment by running
```
pip install https://github.com/Septima/malstroem/archive/master.zip#[speedups]
```
8. Still in the Anaconda Prompt check that malstroem responds to
```
 malstroem --help
 ```

-------------

# Running the example:

To compute the example a DEM has been included for the project area [link](ExamleData/DEMadjusted.tif).
This DEM can be executed using pre-defined bat files, where you have to follow these steps:

1. Open the Anaconda Prompt and run the following command to activate the `malstroem` environment in the current prompt. 
```
conda activate malstroem
```
Your prompt should show something like
```
(malstroem) C:\Users\###>
```
Indicating that `malstroem` is the currently active environment.

2. Open up the [malstroem_init.bat](ExampleData/malstroem_init.bat) and set the path to match the location on your computer:
```
rem Inputs
set DEMFILE="C:\ExampleData\DEMadjusted.tif"
```
3. Navigate to the folder via the prompt using "cd" to obtain the right path in the prompt:
```
(malstroem) C:\Users\###> cd C:\ExampleData
```
4. Run the [malstroem_init.bat](ExampleData/malstroem_init.bat) within the prompt (you may write "m" and then use "tab" to find the bat file

5. Run the [malstroem_run.bat](ExampleData/malstroem_run.bat) within the prompt (you may write "m" and then use "tab" to find the bat file
Now the maltroem model is beeing executed.

6. Once the model has been succesfully completed results may be visualized in QGIS